#To generate a LUT for DF 


include("ArduinoFunctions.jl");
include("DFfunctions.jl");
include("SimpleSoapyLMS7.jl");
using Statistics, StatsBase, Serialization, Plots

desiredStationMHz = 108e6

simValues = deserialize("90.4.LUT") #LUT used for simulated measurements
simTruth = deserialize("90.4.TRUTH")
simTruth = simTruth[2:end]
simValues = simValues[2:end]
simValues *= (2π * desiredStationMHz)

LUT = deserialize("89_1.LUT") #LUT used for correlation
LUT = LUT[2:end]
Truth = deserialize("89_1.TRUTH")
Truth = Truth[2:end]

LUT *= (2π * desiredStationMHz)  #convert back to phase differences

output_loss = zeros(length(simValues))
output = zeros(length(simValues))

gr()
a = 1.2 #for plotting

for i = 1:length(simValues)
    measurement = zeros(2)
    89
    measurement_alt = zeros(2)

    for antenna = 1:2
        mean1 = simValues[i][antenna]


        global measurement[antenna] = mean1  #Δt ;
        global measurement_alt[antenna] = angle(ℯ^(im * (mean1 + pi)))  #Δt_alt ;
    end

    local AoA, Loss = LookUpAngle(LUT, Truth, measurement)
    local AoA_alt, Loss_alt = LookUpAngle(LUT, Truth, measurement_alt)

    output_loss[i] = AoA - simTruth[i]
    output[i] = AoA


end
error = angle.(ℯ .^ (im * ((output - simTruth) * pi / 180))) * 180 / pi
display((plot(scatter(simTruth, output, title="LMSE vs Truth : 90.4 vs 89_1", ylabel="Entry of LMSE (degrees)", xlabel="Truth (degrees)", legend=false)))) # histogram( error,xticks=-185:30:175,bins = range(-180,180, step = 10),title="Error distribution"),layout=(2,1))))


println("std dev of error = " * string(std(error)))
println("mean of error = " * string(mean(error)))












