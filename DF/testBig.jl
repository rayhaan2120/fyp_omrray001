#This is really just a test script
#Do not give much attention to item
#It produces a big plot - hence the name

sampRate = 2048e3; #Sample rate
desiredStationMHz = 108e6 #target frequency
desiredStationMHz2 = desiredStationMHz;
f0 = desiredStationMHz - 0.5e6; #center frequency
buffsz = 1024; #buffer size for each FFT
timeS = 15; #Total length of sampling
d = 0.4 # distance between antennas


#Derived
buff = Array{ComplexF32}(undef, buffsz);
buff2 = Array{ComplexF32}(undef, buffsz);

timeSamp = Int(floor(timeS * sampRate / buffsz)); #Number of buffers to fill in timeS
storeIq = zeros(ComplexF32, buffsz * timeSamp);
storeIq2 = zeros(ComplexF32, buffsz * timeSamp);



include("DFfunctions.jl");
include("SimpleSoapyLMS7.jl");

using SoapySDR, SoapyLMS7_jll, FFTW, DSP, Plots, Statistics

sdr = SoapySDR.SoapySDRDevice_make(Devices()[1])
#Select first device (not exactly the correct way to do things but should always work)

if (SoapySDR.SoapySDRDevice_setSampleRate(sdr, SoapySDR.SOAPY_SDR_RX, 0, sampRate) != 0)
    println("setSampleRate fail: ", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Sample rate 1: ", (sampRate / 1e6), " MHz")

if (SoapySDR.SoapySDRDevice_setSampleRate(sdr, SoapySDR.SOAPY_SDR_RX, 1, sampRate) != 0)
    println("setSampleRate 2 fail: ", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Sample rate 2: ", (sampRate / 1e6), " MHz")


if (SoapySDR.SoapySDRDevice_setFrequency(sdr, SoapySDR.SOAPY_SDR_RX, 0, f0, Devices()[1]) != 0)
    println("setFrequency fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Center frequency 1: ", (f0 / 1e6), " MHz")

if (SoapySDR.SoapySDRDevice_setFrequency(sdr, SoapySDR.SOAPY_SDR_RX, 1, f0, Devices()[1]) != 0)
    println("setFrequency 2 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Center frequency 1: ", (f0 / 1e6), " MHz")


if (SoapySDR.SoapySDRDevice_setGain(sdr, SoapySDR.SOAPY_SDR_RX, 0, 50) != 0)
    println("setGain 1 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Gain 1 set")

if (SoapySDR.SoapySDRDevice_setGain(sdr, SoapySDR.SOAPY_SDR_RX, 1, 50) != 0)
    println("setGain 2 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
end
println("Gain 2 set")


# create a re-usable buffer for rx samples
buffsz = 1024
buff = Array{ComplexF32}(undef, buffsz)
buff2 = Array{ComplexF32}(undef, buffsz)

# receive some samples
timeS = 15 #Total length of sampling
numBuffs = Int(floor(timeS * sampRate / buffsz)) #Number of buffers to fill in timeS
storeIq = zeros(ComplexF32, buffsz * numBuffs)
storeIq2 = zeros(ComplexF32, buffsz * numBuffs)

# Parameters for streaming
flags = Ref{Cint}()
timeNs = Ref{Clonglong}()
buffs = [buff, buff2]

# Array to store IQ data
storeBuff = zeros(ComplexF32, numBuffs, buffsz);
storeBuff2 = zeros(ComplexF32, numBuffs, buffsz);

println("Receiving ", string(timeS), " seconds of data at ", sampRate / 1e6, " MHz = ", numBuffs * buffsz, " samples")
println(numBuffs, " buffers of size ", buffsz)


# set up a stream (complex floats)
rxStream = SoapySDR.SoapySDRDevice_setupStream(sdr, SoapySDR.SOAPY_SDR_RX, SoapySDR.SOAPY_SDR_CF32, [0, 1], 2, Devices()[1])

# start streaming
SoapySDR.SoapySDRDevice_activateStream(sdr, rxStream, 0, 0, 0)


#setup some stuff for plotting
ticks, FreqIndexMHz = makeFreqIndex(1024 * 8 - 1)
index = getIndex(desiredStationMHz, f0, 1024 * 8)
index2 = getIndex(desiredStationMHz2, f0, 1024 * 8)
leftedge = index - 0;
rightedge = index + 0;
if leftedge < 1
    leftedge = 1
end
if rightedge > buffsz * 8
    rightedge = buffsz * 8
end

# rolling average stuff
qlen = 1
q = [zeros(buffsz * 8) * 1im for _ in 1:qlen]




gr()

default(titlefont=(6, "helvetica"), legendfontsize=7, guidefont=(5, :darkgreen), tickfont=(3, :black))


for i = 1:timeSamp*100
    SoapySDR.SoapySDRDevice_readStream(sdr, rxStream, buffs, buffsz, flags, timeNs, 1)
    #buff.-=mean(buff)
    #buff2.-=mean(buff2)
    #buff=[buff;zeros(1024*7)]
    #buff2 = [buff2;zeros(1024*7)]
    buff .*= blackman(1024)
    buff2 .*= blackman(1024)
    tempbuff = [buff; zeros(1024 * 7)]
    tempbuff2 = [buff2; zeros(1024 * 7)]
    #local storeBuff[i,:] = buff
    # local storeBuff2[i,:] = buff2
    #IJulia.clear_output(true)
    FFTProduct = fftshift(MatFftProduct(fft(tempbuff), fft(tempbuff2)))


    # iqplot = plot(real(buff),ylims=(-0.1,0.1) )
    # plot!(imag(buff),ylims=(-0.1,0.11))

    qPos = i % qlen + 1
    q[qPos] = FFTProduct
    FFTProduct = mean.(eachcol(q))[1]

    #angleplot = histogram2d(FreqIndexMHz,angle.(MatFftProduct(fftshift(fft(buff)),fftshift(fft(buff2)))),ylims=(-pi,pi),bins=35,legend=false)
    # magplot = plot(FreqIndexMHz,20 .*log10.(abs.(FFTProduct)),ylims=(-100,100))

    #magplot = plot(FreqIndexMHz,abs.((MatFftProduct(fftshift(fft(buff)),fftshift(fft(buff2))))),ylims=(0,10))
    #value,index = findmax(abs.(FFTProduct))
    #magplot = plot(FreqIndexMHz,abs.(((fftshift(fft(buff))))),ylims=(0,2))
    #plot!(FreqIndexMHz,abs.(((fftshift(fft(buff2))))),ylims=(0,2))

    chunk = FFTProduct[leftedge:rightedge]
    global chunkAvg = mean(chunk)
    chunkAvg = chunkAvg / abs(chunkAvg)
    #chunk=chunk./abs.(chunk)

    a = 1.2
    #phasorplot = scatter(real(chunk),imag(chunk),ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)
    # for n = 1:length(chunk)
    #   plot!([0,real(chunk[n])],[0,imag(chunk[n])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)
    # end 

    #angleplot = plot(FreqIndexMHz,(angle.(FFTProduct))*180/pi,ylims=(-180,180),markersize=2,seriestype=:scatter,title=round((correctOffset(angle(chunkAvg)+pi/2,pi/2)%pi)*180/pi))
    #scatter!([FreqIndexMHz[index]],[angle(chunkAvg)*180/pi],markersize=5)
    #scatter!([FreqIndexMHz[index2]],[angle(FFTProduct[index2])*180/pi],markersize=5)
    if (i % 100 == 0)
        phasorplot = scatter(real([chunkAvg]), imag([chunkAvg]), ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal, title=round(angle(chunkAvg) * 180 / pi))
        plot!([0, real(chunkAvg)], [0, imag(chunkAvg)], ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal)

        display(plot(phasorplot, legend=false))
    end
    #scatter!(real([FFTProduct[index2]]),imag([FFTProduct[index2]]))
    #plot!([0,real(FFTProduct[index2])],[0,imag(FFTProduct[index2])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)


    #phasorplot2 = scatter(-imag([chunkAvg]),real([chunkAvg]),ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal,title="corrected Δphase :"*string(round(angle(chunkAvg)*180/pi+90)))

    #estimate = AoA((correctOffset(angle(chunkAvg)+pi/2,pi/2)%pi),desiredStationMHz,d)

    #AoAPlot = scatter([cos(estimate)],[sin(estimate)],ylims=(-1.2,1.2),xlims=(-1.2,1.2),aspect_ratio=:equal,title="AoA:"*string(round(estimate*180/pi,digits=2)))
    #plot!([0,cos(estimate)],[0,sin(estimate)],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)

    #sleep(0.1)


    #sleep(0.1)

end


# shutdown the stream
#NB comment out lines 568-570 in auto_wrap.jl for deactivateStream to work
# ~/.julia/packages/SoapySDR/nBGlm/src/lowlevel/auto_wrap.jl
SoapySDR.SoapySDRDevice_deactivateStream(sdr, rxStream, 0, 0)  # stop streaming
SoapySDR.SoapySDRDevice_closeStream(sdr, rxStream);

SoapySDR.SoapySDRDevice_unmake(sdr);