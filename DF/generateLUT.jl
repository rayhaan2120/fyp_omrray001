#To generate a LUT for DF 


include("ArduinoFunctions.jl");
include("DFfunctions.jl");
include("SimpleSoapyLMS7.jl");
using Statistics, StatsBase, Serialization


Arduino = true
sampRate = 2048e3; #Sample rate and bandwidth
desiredStationMHz = 89.0e6
desiredStationMHz2 = 88.5e6 #desiredStationMHz - 0.75

f0 = desiredStationMHz - 0.5e6; #center frequency

fTx = desiredStationMHz2; #transmit frequency

buffsz = 1024; #buffer size for each FFT
timeS = 1; #Total length of sampling

zeroPaddingMultiplier = 2;
zeroPadding = zeroPaddingMultiplier - 1;
buffszPadded = zeroPaddingMultiplier * buffsz;

timeSamp = Int(floor(timeS * sampRate / buffsz)); #Number of buffers to fill in timeS

#Connect to Arduino for controlloing switch
if Arduino
    ard = connectArduino()
end

# create a re-usable buffer for rx samples
buff = Array{ComplexF32}(undef, buffsz)
buff2 = Array{ComplexF32}(undef, buffsz)


#create reu-usable buffer for tx
buffTx = Array{ComplexF32}(undef, buffsz)
buffTx = createTone(sampRate, fTx, buffsz)
buffTx = [convert(typeof(buff), buffTx)];

# Parameters for storing IQ data in Arrays
numBuffs = Int(floor(timeS * sampRate / buffsz)) #Number of buffers to fill in timeS

# Parameters for streaming
flags = Ref{Cint}()
timeNs = Ref{Clonglong}()
buffs = [buff, buff2]


Lime = connectAndSetupLime(sampRate, true, true, true, f0, f0, fTx)

rxStream, txStream = setupStream(Lime)

ticks, FreqIndexMHz = makeFreqIndex(buffszPadded - 1)

LUT = Vector{Vector{Float64}}();
Truth = Vector{Int64}()
println("Enter the number of entries for this LUT:")
input = readline()


numEntries = parse(Int64, input)
stepSize = div(360, numEntries)
steps = -stepSize:stepSize:360-stepSize #Include an extra measurement at the beginning due to the first readings always containing errors


#Increment from 0 to 360° for the number of steps specified. 
for truthangle = steps
    push!(Truth, truthangle)
    meanAngles = zeros(2)
    stdDevs = [zeros(2) for _ in 1:2]
    LUTentry = zeros(2)
    if Arduino
        selectA(ard)
    end


    #There are three antennas Rx1,Rx2 and Rx3,
    #Rx1 is a part of both antenna pairs which can be sampled simultaneously 
    for antenna = 1:2

        # For iterative standard deviation and mean
        global sum1 = 0 * im
        global sum2 = zeros(buffszPadded) * im
        global sumSqr1 = 0 * im
        global sumSqr2 = 0 * im

        for i = 1:numBuffs
            readStream(Lime, rxStream, buffs, buffsz)
            tempfft1 = fftshift(fft([buff; zeros(buffsz)]))
            tempfft2 = fftshift(fft([buff2; zeros(buffsz)]))
            global FFTProduct = MatFftProduct(tempfft1, tempfft2)

            #println(size(FFTProduct))
            if (i == 1)
                global fft1 = FFTProduct
                global peakIndex = getFMStationPeakIndex(FFTProduct, buffsz, FreqIndexMHz, desiredStationMHz / 1e6)
                global peakIndex2 = getFMStationPeakIndex(FFTProduct, buffsz, FreqIndexMHz, desiredStationMHz2 / 1e6)
            end

            global sum1 += FFTProduct[peakIndex]

        end

        meanAngleDif = MatAngleDif([sum1], 1) #Find angleDifference
        meanAngles[antenna] = meanAngleDif
        Δt = meanAngleDif / (2π * desiredStationMHz)
        global LUTentry[antenna] = Δt #Fill entry for LUT


        if Arduino
            selectB(ard)
        end #switch to next antenna pair

    end

    println()
    println("mean 1:    ", meanAngles[1] * 180 / pi)
    println()
    println("mean 2:    ", meanAngles[2] * 180 / pi)
    println()

    #Add entry to LUT
    push!(LUT, LUTentry)

    println("Completed LUT entry for " * string(truthangle) * " degrees.")
    if ((truthangle + stepSize) < 360)
        println("Press enter to begin measurement for " * string(truthangle + stepSize) * " degrees :")
        global input = readline()
    end

end


if Arduino
    disconnectArduino(ard)
end

#Save LUT
serialize(string(desiredStationMHz / 1e6) * ".LUT", LUT)
serialize(string(desiredStationMHz / 1e6) * ".TRUTH", Truth)

disconnectLime(Lime, rxStream, txStream)

#Save complex and absolute info seperately (for convenience)







