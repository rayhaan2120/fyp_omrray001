## A lot fo these functions are no longer used

#Return the xticks and xticklabels for plotting given the center frequency 
#(may have to convert the index to strings)
function makeFreqIndex(numticks=buffsz,f0=f0,sampRate=sampRate,scale=1e6)
    minF = (f0 - sampRate/2 )./ scale;
    maxF = (f0 + sampRate/2 )./ scale;
    
    return ((0:1:numticks) , (round.((minF:(maxF-minF)/numticks:maxF),digits=2)));
end


#returns the smallest angle required to get to a2 from a1
function angleDif(a1,a2)
    offset = 0
    if a1 - a2 > pi
        offset = 2pi
    elseif (a1 - a2 < -pi)
            offset = -2pi
        end
    return a2 - a1 + offset
    
end

#same as AngleDif but for an entire fft vector
function MatAngleDif(cmplxfft1,cmplxfft2,index)
    FFTProduct = MatFftProduct(cmplxfft1,cmplxfft2)
    
    return angle.(FFTProduct[:,index])   
end

#overloaded version for a predone FFTproduct
function MatAngleDif(FFTProduct ,index)
    if (length(size(FFTProduct))>1)
        return angle.(FFTProduct[:,index])
    else
        return angle(FFTProduct[index])
    end
end
    
#for the mean
function MatAngleDifMean(FFTProduct ,index)
    return angle.(mean(FFTProduct[:,index]))   
end


#get FFT product
function MatFftProduct(cmplxfft1,cmplxfft2)
        return cmplxfft1.*conj(cmplxfft2)
end

# Get the index of the peak magnitude of a desired radio station
function getFMStationPeakIndex(FFTProduct,buffsz,FreqIndexMHz,desiredStationMHz)
   
    if desiredStationMHz - 0.1  >= FreqIndexMHz[1]
 
         start = findall(x -> x == round(desiredStationMHz - 0.1, digits =3), FreqIndexMHz)[1]
     else
 
         start = 1;
     end
 
    if desiredStationMHz + 0.1 <= last(FreqIndexMHz)
 
        stop = findall(x -> x == round(desiredStationMHz + 0.1, digits =3), FreqIndexMHz)[1]    
     else
         stop = length(FreqIndexMHz)-1;
     end
     
    if (length(size(FFTProduct))>1)
        value,index = findmax(mean.(eachcol(abs.(FFTProduct)))[start:stop])
     else 
        value,index = findmax(((abs.(FFTProduct)))[start:stop])
     end
     index+=start-1
    

     return index
    
 end
     

# Get index of radio station
 function getIndex(desiredStationMHz,f0=f0,buffsz=buffsz,sampRate=sampRate,scale=1e6)
    minF = (f0 - sampRate/2 )./ scale;
    maxF = (f0 + sampRate/2 )./ scale;
    freqPerIndex=(maxF-minF)/buffsz
    output = ((desiredStationMHz/scale-minF)/freqPerIndex)
    if output>buffsz
        println("getIndex() out of range - clamping index down to ",buffsz," to fit bounds")
    end
    
      if output<1
        println("getIndex() out of range - clamping index up to 1 to fit bounds")
    end
        
   return  clamp(Integer(round(output)),1,buffsz)
end


# Create tone for transmitting of buffer size of buffsz at frequency f0
function createTone(fs,f0,buffsz)
    complexToneBuffer = Array{ComplexF32}(undef, buffsz);
    period = fs/f0
    ωt = 2*pi*f0*(0:1:buffsz-1)/fs
    complexToneBuffer = exp.(1im*ωt)
    return complexToneBuffer
end

#Find moving average for vector, vs, with kernel width of n
moving_average(vs,n) = [mean(@view vs[i:(i+n-1)]) for i in 1:(length(vs)-(n-1))]


function calibrationTimeDelay(referencePhaseDiff,referenceFreq,f0)
    f = referenceFreq - f0;
    t = referencePhaseDiff/(2*pi*f)
    return t

end

function calibrationExponential(referenceTimeDelay,targetFreq,f0)
    # f = targetFreq - f0;
    # w = 2*pi*f
    # return exp(-w*im*referenceTimeDelay)

    return exp(-2*pi*(targetFreq-f0)*im*referenceTimeDelay)
end

function calibrationExponentialArray(referenceTimeDelay,buffsz,sampRate,f0)
    ticks,index = makeFreqIndex(buffsz-1,f0,sampRate,1e0)
    return calibrationExponential.(referenceTimeDelay,index,f0)
end

function correctOffset(angle,upperbound=pi/2,lowerbound=upperbound)
    if angle>upperbound #should be pi/2 but set it a bit higher for greater tolerance
        angle-=pi
    elseif angle<-lowerbound
        angle+=pi
    end

    return angle 
end

function AoA(Δphase,freq,d)

    return acos(clamp(Δphase*3e8/(2pi*d*freq),-1,1))
end


function AoA(Δphase,coeffecient)
    return acos(clamp(Δphase*coeffecient,-1,1))
end

function MSE(x,y)
    return sum((x.-y).^2)/length(x)
 end

 function MSEe(x,y)
    return sum((angle.(ℯ.^(im*(x.-y)))).^2)/length(x)
 end


 function LookUpAngle(LUT,Truth,measurement)

    maxcor = 2^32;
    maxcorindex = 1;    
    LOSS = zeros(length(LUT))
        for i in 1:length(LUT)
        # value = MSE(LUT[i],measurement);
        value = MSEe(LUT[i],measurement);
        LOSS[i]= value;
        if value<maxcor
            maxcor = value;
            maxcorindex = i;
            
        end
        
    end

    return Truth[maxcorindex],LOSS

end