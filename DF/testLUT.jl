#To use a pre-generateed LUT for DF 


include("ArduinoFunctions.jl");
include("DFfunctions.jl");
include("SimpleSoapyLMS7.jl");
using Statistics, StatsBase, Serialization, Plots


Arduino = true
sampRate = 2048e3; #Sample rate and bandwidth
desiredStationMHz = 89.0e6
desiredStationMHz2 = 88.5e6 #desiredStationMHz - 0.75

f0 = desiredStationMHz - 0.5e6; #center frequency

fTx = desiredStationMHz2; #transmit frequency

buffsz = 1024; #buffer size for each FFT
timeS = 1; #Total length of sampling

zeroPaddingMultiplier = 2;
zeroPadding = zeroPaddingMultiplier - 1;
buffszPadded = zeroPaddingMultiplier * buffsz;

timeSamp = Int(floor(timeS * sampRate / buffsz)); #Number of buffers to fill in timeS

#Connect to Arduino for controlloing switch
if Arduino
    ard = connectArduino()
end

# create a re-usable buffer for rx samples
buff = Array{ComplexF32}(undef, buffsz)
buff2 = Array{ComplexF32}(undef, buffsz)


#create reu-usable buffer for tx
buffTx = Array{ComplexF32}(undef, buffsz)
buffTx = createTone(sampRate, fTx, buffsz)
buffTx = [convert(typeof(buff), buffTx)];

# Parameters for storing IQ data in Arrays
numBuffs = Int(floor(timeS * sampRate / buffsz)) #Number of buffers to fill in timeS

# Parameters for streaming
flags = Ref{Cint}()
timeNs = Ref{Clonglong}()
buffs = [buff, buff2]


Lime = connectAndSetupLime(sampRate, true, true, true, f0, f0, fTx)

rxStream, txStream = setupStream(Lime)

ticks, FreqIndexMHz = makeFreqIndex(buffszPadded - 1)

LUT = deserialize("89.0.LUT")
LUT = LUT[2:end]
Truth = deserialize("89.0.TRUTH")
Truth = Truth[2:end]

LUT *= (2π * desiredStationMHz)  #convert back to phase differences

gr()
a = 1.2 #for plotting

while 1 == 1

    meanAngles = zeros(2)
    stdDevs = [zeros(2) for _ in 1:2]
    measurement = zeros(2)
    measurement_alt = zeros(2)
    if Arduino
        selectA(ard)
    end

    for antenna = 1:2

        # For iterative standard deviation and mean
        global sum1 = 0
        global sum2 = 0
        global sumSqr1 = 0 * im
        global sumSqr2 = 0 * im

        for i = 1:numBuffs
            readStream(Lime, rxStream, buffs, buffsz)
            tempfft1 = fftshift(fft([buff; zeros(buffsz)]))
            tempfft2 = fftshift(fft([buff2; zeros(buffsz)]))
            global FFTProduct = MatFftProduct(tempfft1, tempfft2)

            #println(size(FFTProduct))
            #  if (i==1)
            #global fft1 = FFTProduct;
            # global peakIndex = getIndex(desiredStationMHz,f0,buffszPadded,sampRate)
            # global peakIndex2 = getIndex(desiredStationMHz2,f0,buffszPadded,sampRate)
            global peakIndex = getFMStationPeakIndex(FFTProduct, buffsz, FreqIndexMHz, desiredStationMHz / 1e6)
            global peakIndex2 = getFMStationPeakIndex(FFTProduct, buffsz, FreqIndexMHz, desiredStationMHz2 / 1e6)
            # end
            #AngleDif2 = MatAngleDif(FFTProduct, peakIndex2);

            global sum1 += FFTProduct[peakIndex]

            #println(100*i/numBuffs)
        end

        mean1 = MatAngleDif([sum1], 1)
        meanAngles[antenna] = mean1
        Δt = mean1 / (2π * desiredStationMHz)
        Δt_alt = (angle(ℯ^(im * (mean1 + pi)))) / (2π * desiredStationMHz)

        global measurement[antenna] = mean1  #Δt ;
        global measurement_alt[antenna] = angle(ℯ^(im * (mean1 + pi)))  #Δt_alt ;


        if Arduino
            selectB(ard)
        end
    end


    AoA, Loss = LookUpAngle(LUT, Truth, measurement)
    AoA_alt, Loss_alt = LookUpAngle(LUT, Truth, measurement_alt)

    # global phasorplot = scatter([cos(meanAngles[1])],[sin(meanAngles[1])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal,title="Δphase :"*string(round((meanAngles[1])*180/pi))*"°")
    # plot!([0,cos(meanAngles[1])],[0,sin(meanAngles[1])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)

    # global phasorplot2 = scatter([cos(meanAngles[2])],[sin(meanAngles[2])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal,title="Δphase :"*string(round((meanAngles[2])*180/pi))*"°")
    # plot!([0,cos(meanAngles[2])],[0,sin(meanAngles[2])],ylims=(-a,a),xlims=(-a,a),aspect_ratio=:equal)


    global phasorplot = scatter([cosd(AoA)], [sind(AoA)], ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal, title="Δphase :" * string(round(AoA)) * "°")
    plot!([0, cosd(AoA)], [0, sind(AoA)], ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal)

    global phasorplot2 = scatter([cosd(AoA_alt)], [sind(AoA_alt)], ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal, title="Δphase :" * string(AoA_alt) * "°")
    plot!([0, cosd(AoA_alt)], [0, sind(AoA_alt)], ylims=(-a, a), xlims=(-a, a), aspect_ratio=:equal)


    display(plot(phasorplot, phasorplot2))
    println()
    println("mean 1:    ", meanAngles[1] * 180 / pi)
    println()
    println("mean 2:    ", meanAngles[2] * 180 / pi)
    println()
    println("AoA: " * string(AoA))
    println("AoA alt: " * string(AoA_alt))
    println()
    sleep(1)


end
if Arduino
    disconnectArduino(ard)
end


disconnectLime(Lime, rxStream, txStream)

#Save complex and absolute info seperately (for convenience)







