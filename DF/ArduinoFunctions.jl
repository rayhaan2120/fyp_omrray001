using LibSerialPort

function connectArduino()
    return Arduino = open("/dev/ttyACM0", 9600)
    selectA(Arduino)
end

function disconnectArduino(Arduino)
    unselect(Arduino)
    close(Arduino)
end

function selectA(Arduino)
    write(Arduino, "A")
    sleep(2)
    BytesAvailable = bytesavailable(Arduino)   # Number of bytes available in the buffer 
    x = zeros(UInt8, BytesAvailable)  # Create an Uint8 array into which to read the  bytes.
    for n = 1:BytesAvailable
        x[n] = read(Arduino, UInt8)
        print(Char(Int(x[n])))
    end
end

function selectB(Arduino)
    write(Arduino, "B")
    sleep(2)
    BytesAvailable = bytesavailable(Arduino)   # Number of bytes available in the buffer 
    x = zeros(UInt8, BytesAvailable)  # Create an Uint8 array into which to read the  bytes.
    for n = 1:BytesAvailable
        x[n] = read(Arduino, UInt8)
        print(Char(Int(x[n])))
    end
end

function unselect(Arduino)
    write(Arduino, "N")
    sleep(0.5)
    BytesAvailable = bytesavailable(Arduino)   # Number of bytes available in the buffer 
    x = zeros(UInt8, BytesAvailable)  # Create an Uint8 array into which to read the  bytes.
    for n = 1:BytesAvailable
        x[n] = read(Arduino, UInt8)
        print(Char(Int(x[n])))
    end
end

