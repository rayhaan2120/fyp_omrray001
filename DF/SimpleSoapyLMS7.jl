#This is collection of functions to simplify the process of using Soapy with Julia 1.8.0+ for my direction finding project
#Author Rayhaan Omar
#Date: 16 October 2022

using SoapySDR, SoapyLMS7_jll, FFTW, DSP;


function connectLime()
    return SoapySDR.SoapySDRDevice_make(Devices()[1])
end

function setupLime(sdr, sampleRate=2048e3, Rx0=true, Rx1=true, Tx=true, f0_Rx0=107.5e6, f0_Rx1=f0_Rx0, f0_Tx=f0_Rx0 - 0.5e6, gain_Rx0=50, gain_Rx1=50, gain_Tx=120)

    if (Rx0)
        if (SoapySDR.SoapySDRDevice_setSampleRate(sdr, SoapySDR.SOAPY_SDR_RX, 0, sampRate) != 0)
            println("setSampleRate fail: ", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Sample rate 1: ", (sampRate / 1e6), " MHz")

        if (SoapySDR.SoapySDRDevice_setFrequency(sdr, SoapySDR.SOAPY_SDR_RX, 0, f0_Rx0, Devices()[1]) != 0)
            println("setFrequency fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Center frequency 1: ", (f0 / 1e6), " MHz")

        if (SoapySDR.SoapySDRDevice_setGain(sdr, SoapySDR.SOAPY_SDR_RX, 0, gain_Rx0) != 0)
            println("setGain 1 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Rx Gain 1 set")

    end

    if (Rx1)

        if (SoapySDR.SoapySDRDevice_setSampleRate(sdr, SoapySDR.SOAPY_SDR_RX, 1, sampRate) != 0)
            println("setSampleRate 2 fail: ", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Sample rate 2: ", (sampRate / 1e6), " MHz")

        if (SoapySDR.SoapySDRDevice_setFrequency(sdr, SoapySDR.SOAPY_SDR_RX, 1, f0_Rx1, Devices()[1]) != 0)
            println("setFrequency 2 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Center frequency 1: ", (f0 / 1e6), " MHz")

        if (SoapySDR.SoapySDRDevice_setGain(sdr, SoapySDR.SOAPY_SDR_RX, 1, gain_Rx1) != 0)
            println("setGain 2 fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Rx Gain 2 set")

    end



    if (Tx)

        if (SoapySDR.SoapySDRDevice_setSampleRate(sdr, SoapySDR.SOAPY_SDR_TX, 0, sampRate) != 0)
            println("setSampleRate TX fail: ", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Sample rate TX: ", (sampRate / 1e6), " MHz")


        if (SoapySDR.SoapySDRDevice_setFrequency(sdr, SoapySDR.SOAPY_SDR_TX, 0, f0_Tx, Devices()[1]) != 0)
            println("setFrequency TX fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Center frequency Tx: ", (fTx / 1e6), " MHz")


        if (SoapySDR.SoapySDRDevice_setGain(sdr, SoapySDR.SOAPY_SDR_TX, 0, gain_Tx) != 0)
            println("setGain Tx fail", unsafe_string(SoapySDR.SoapySDRDevice_lastError()))
        end
        println("Tx Gain set to", string(gain_Tx))

    end
end



function connectAndSetupLime(sampleRate=2048e3, Rx0=true, Rx1=true, Tx=true, f0_Rx0=107.5e6, f0_Rx1=f0_Rx0, f0_Tx=f0_Rx0 - 0.5e6, gain_Rx0=50, gain_Rx1=50, gain_Tx=120)
    sdr = connectLime()
    setupLime(sdr, sampleRate, Rx0, Rx1, Tx, f0_Rx0, f0_Rx1, f0_Tx, gain_Rx0, gain_Rx1, gain_Tx)
    return sdr
end


function setupStream(sdr, Rx=true, Tx=true)
    # set up a stream (complex floats)
    if (Rx)
        rxStream = SoapySDR.SoapySDRDevice_setupStream(sdr, SoapySDR.SOAPY_SDR_RX, SoapySDR.SOAPY_SDR_CF32, [0, 1], 2, Devices()[1])
    end
    if (Tx)
        txStream = SoapySDR.SoapySDRDevice_setupStream(sdr, SoapySDR.SOAPY_SDR_TX, SoapySDR.SOAPY_SDR_CF32, C_NULL, 0, Devices()[1])
    end

    # Activate Streams
    if (Rx)
        SoapySDR.SoapySDRDevice_activateStream(sdr, rxStream, 0, 0, 0)
    end

    if (Tx)
        SoapySDR.SoapySDRDevice_activateStream(sdr, txStream, 0, 0, 0)
    end

    if (Rx & Tx)
        return rxStream, txStream
    elseif (Rx)
        return rxStream
    elseif (Tx)
        return txStream
    else
        println("Error: Rx & Tx set to false.")
        return 1
    end
end

function sampleForTime(sdr, timeS, rxStream, buffs, buffsz, storeBuff, storeBuff2)
    flags = Ref{Cint}()
    timeNs = Ref{Clonglong}()
    window = blackman(buffsz)
    numBuffs = Int(floor(timeS * sampRate / buffsz)) #Number of buffers to fill in timeS
    for i = 1:numBuffs
        SoapySDR.SoapySDRDevice_readStream(sdr, rxStream, buffs, buffsz, flags, timeNs, 100000)
        global storeBuff[i, :] = buff .* window
        global storeBuff2[i, :] = buff2 .* window
    end
end

function sampleAndTxForTime(sdr, timeS, rxStream, txStream, buffs, buffTx, buffsz, storeBuff, storeBuff2)
    flags = Ref{Cint}()
    timeNs = Ref{Clonglong}()
    window = blackman(buffsz)
    numBuffs = Int(floor(timeS * sampRate / buffsz)) #Number of buffers to fill in timeS
    for i = 1:numBuffs
        SoapySDR.SoapySDRDevice_writeStream(sdr, txStream, buffTx, buffsz, flags, timeNs[], 100000)
        SoapySDR.SoapySDRDevice_readStream(sdr, rxStream, buffs, buffsz, flags, timeNs, 100000)
        global storeBuff[i, :] = buff#.*window
        global storeBuff2[i, :] = buff2#.*window
    end
end

function readStream(sdr, rxStream, buffs, buffsz)
    flags = Ref{Cint}()
    timeNs = Ref{Clonglong}()
    window = blackman(buffsz)
    SoapySDR.SoapySDRDevice_readStream(sdr, rxStream, buffs, buffsz, flags, timeNs, buffsz)
end

function writeStream(sdr, txStream, buffTx, buffsz)
    flags = Ref{Cint}()
    timeNs = Ref{Clonglong}()
    SoapySDR.SoapySDRDevice_writeStream(sdr, txStream, buffTx, buffsz, flags, timeNs[], 100000)
end

function readWriteStream(sdr, rxStream, txStream, buffs, buffTx, buffsz)
    flags = Ref{Cint}()
    timeNs = Ref{Clonglong}()
    window = blackman(buffsz)

    SoapySDR.SoapySDRDevice_writeStream(sdr, txStream, buffTx, buffsz, flags, 0, 100000)
    SoapySDR.SoapySDRDevice_readStream(sdr, rxStream, buffs, buffsz, flags, timeNs, 100000)
end


function disconnectLime(sdr, rxStream, txStream)

    # shutdown the stream
    #NB comment out lines 568-570 in auto_wrap.jl for deactivateStream to work
    # ~/.julia/packages/SoapySDR/nBGlm/src/lowlevel/auto_wrap.jl
    SoapySDR.SoapySDRDevice_deactivateStream(sdr, rxStream, 0, 0)  # stop streaming
    SoapySDR.SoapySDRDevice_closeStream(sdr, rxStream)

    SoapySDR.SoapySDRDevice_deactivateStream(sdr, txStream, 0, 0)  # stop streaming
    SoapySDR.SoapySDRDevice_closeStream(sdr, txStream)

    SoapySDR.SoapySDRDevice_unmake(sdr) #delete pointer to SDR
end




