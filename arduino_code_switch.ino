char input;
 
void setup() {
    Serial.begin(9600); 
    delay(2000);  
    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);
    digitalWrite(0, LOW);
    digitalWrite(1, LOW);
    Serial.println("Send A for Antenna 1, B for Antenna 2, N for neither");
}
 
void loop() {
    if(Serial.available()){
        input = Serial.read();
        if (input == 'A') {
          digitalWrite(0, HIGH);
          digitalWrite(1, LOW);
          Serial.println("Antenna 1 selected");
          }
          else if (input == 'B'){
            digitalWrite(0, LOW);
            digitalWrite(1, HIGH);
            Serial.println("Antenna 2 selected");
            }

           else if (input == 'N'){
            digitalWrite(0 ,LOW);
            digitalWrite(1 ,LOW);
            Serial.println("Both Antennas off");
           }
           else {
            Serial.println("Invalid input");
            }
           }
        
    }
