using Serialization, Plots

 l=deserialize("89.0.LUT")

l=l[2:end]

a,b=zeros(length(l)),zeros(length(l));

for i=1:length(l)
       a[i]=l[i][1]
       b[i]=l[i][2]
       end

plot(circshift(a,0))
plot!(circshift(b,0))

